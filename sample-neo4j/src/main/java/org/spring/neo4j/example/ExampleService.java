package org.spring.neo4j.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link Service} with hard-coded input data.
 */
@Component
public class ExampleService implements Service {

	@Autowired
	PersonRepository personRepository;

	/**
	 * Reads next record from input
	 */
	public String getMessage() {
		return "Hello world!";
	}

	@Override
	@Transactional
	public Person save(Person person) {
		return personRepository.save(person);
	}

	@Override
	public Person getByName(String name) {
		return personRepository.findByName(name);
	}

}
