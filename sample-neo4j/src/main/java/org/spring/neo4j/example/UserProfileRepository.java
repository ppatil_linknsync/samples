/**
 * 
 */
package org.spring.neo4j.example;

import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * @author pareshpatil
 *
 */
public interface UserProfileRepository extends GraphRepository<UserProfile> {

	UserProfile findByUid(String uid);

	UserProfile findByName(String name);
}
