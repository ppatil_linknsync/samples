/**
 * 
 */
package org.spring.neo4j.example;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author pareshpatil
 *
 */
@NodeEntity
public class UserProfile {

	@GraphId
	private Long id;

	private String name;
	private String uid;

	@Relationship(type = "CONNECTED", direction = Relationship.UNDIRECTED)
	private Set<UserProfile> connections = new HashSet<UserProfile>();

	public UserProfile() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void connections(UserProfile userProfile) {
		connections.add(userProfile);
	}
}
