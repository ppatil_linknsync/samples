/**
 * 
 */
package org.spring.neo4j.example;

/**
 * @author pareshpatil
 *
 */
public interface UserProfileService {

	UserProfile save(UserProfile userProfile);

	UserProfile getByUid(String uid);

	UserProfile getByName(String name);

	UserProfile connect(UserProfile userProfile, UserProfile userProfileToConnectWith);
}
