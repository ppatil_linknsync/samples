/**
 * 
 */
package org.spring.neo4j.example;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author pareshpatil
 *
 */
@Configuration
@ComponentScan(basePackages = { "org.spring.neo4j.example" })
@EnableNeo4jRepositories(basePackages = "org.spring.neo4j.example")
@EnableTransactionManagement
public class MyNeo4jConfiguration {

	@Bean
	public org.neo4j.ogm.config.Configuration configuration() {
		org.neo4j.ogm.config.Configuration config = new org.neo4j.ogm.config.Configuration();
		config.driverConfiguration().setDriverClassName("org.neo4j.ogm.drivers.http.driver.HttpDriver")
				.setCredentials("neo4j", "Linknsync11#11").setURI("http://localhost:7474");
		return config;
	}

	@Bean
	public SessionFactory sessionFactory() {
		// with domain entity base package(s)
		return new SessionFactory(configuration(), "org.spring.neo4j.example");
	}

	@Bean
	public Neo4jTransactionManager transactionManager() {
		return new Neo4jTransactionManager(sessionFactory());
	}
}
