package org.spring.neo4j.example;

public interface Service {

	String getMessage();

	Person save(Person person);

	Person getByName(String name);
}
