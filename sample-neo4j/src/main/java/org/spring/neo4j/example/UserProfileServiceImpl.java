/**
 * 
 */
package org.spring.neo4j.example;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author pareshpatil
 *
 */
@org.springframework.stereotype.Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	UserProfileRepository userProfileRepository;

	@Override
	public UserProfile save(UserProfile userProfile) {
		return userProfileRepository.save(userProfile);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.spring.neo4j.example.UserProfileService#getByUid(java.lang.String)
	 */
	@Override
	public UserProfile getByUid(String uid) {
		return userProfileRepository.findByUid(uid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.spring.neo4j.example.UserProfileService#getByName(java.lang.String)
	 */
	@Override
	public UserProfile getByName(String name) {
		return userProfileRepository.findByName(name);
	}

	@Override
	public UserProfile connect(UserProfile userProfile, UserProfile userProfileToConnectWith) {
		UserProfile upResponse = userProfileRepository.findByName(userProfile.getName());
		upResponse.connections(userProfileToConnectWith);
		return userProfileRepository.save(upResponse);
	}

}
