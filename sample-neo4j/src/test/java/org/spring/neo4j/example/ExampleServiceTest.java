/**
 * 
 */
package org.spring.neo4j.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 * @author pareshpatil
 *
 */
@ContextConfiguration("classpath:META-INF/spring/app-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class ExampleServiceTest {

	@Autowired
	Service service;

	/**
	 * Test method for
	 * {@link org.spring.neo4j.example.ExampleService#getMessage()}.
	 */
	@Test
	public void testGetMessage() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.spring.neo4j.example.ExampleService#save(org.spring.neo4j.example.Person)}
	 * .
	 */
	@Test
	public void testSave() {
		Person greg = new Person("Greg");
		Person responsePerson = service.save(greg);
		assertEquals("Greg", responsePerson.getName());
	}

	/**
	 * Test method for
	 * {@link org.spring.neo4j.example.ExampleService#getByName(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetByName() {
		fail("Not yet implemented");
	}

}
