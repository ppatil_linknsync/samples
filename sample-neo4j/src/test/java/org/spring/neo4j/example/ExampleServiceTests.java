package org.spring.neo4j.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

//@ContextConfiguration("classpath:META-INF/spring/app-context.xml")
@ContextConfiguration(classes = MyNeo4jConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class ExampleServiceTests extends AbstractJUnit4SpringContextTests {
	// AbstractJUnit4SpringContextTests
	// AbstractTransactionalJUnit4SpringContextTests

	@Autowired
	private Service service;

	@Test
	public void testReadOnce() throws Exception {
		assertEquals("Hello world!", service.getMessage());
	}

	@Test
	public void testSave() throws Exception {
		Person greg = new Person("Greg");
		Person responsePerson = service.save(greg);
		assertEquals("Greg", responsePerson.getName());
	}
}
