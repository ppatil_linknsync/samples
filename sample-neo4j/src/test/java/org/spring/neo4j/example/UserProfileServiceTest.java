package org.spring.neo4j.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(classes = MyNeo4jConfiguration.class)
public class UserProfileServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	UserProfileService userProfileService;

	@Test
	public void testSave() {
		UserProfile userProfile = new UserProfile();
		userProfile.setUid("1234");
		userProfile.setName("AA");

		UserProfile userProfile1 = new UserProfile();
		userProfile1.setUid("2345");
		userProfile1.setName("BB");

		userProfileService.save(userProfile);
		userProfileService.save(userProfile1);

		UserProfile a = userProfileService.getByName(userProfile.getName());
		a.connections(userProfile1);
		userProfileService.save(a);
	}

	@Test
	public void testConnect() {
		UserProfile userProfile = new UserProfile();
		userProfile.setUid("1234");
		userProfile.setName("AA");

		UserProfile userProfile1 = new UserProfile();
		userProfile1.setUid("2345");
		userProfile1.setName("BB");

		userProfileService.save(userProfile);
		userProfileService.save(userProfile1);

		UserProfile response = userProfileService.connect(userProfile, userProfile1);
	}

	@Test
	public void testGetByUid() {
		UserProfile userProfile = new UserProfile();
		userProfile.setUid("1234");
		userProfile.setName("AA");
		userProfileService.save(userProfile);

		UserProfile response = userProfileService.getByUid("1234");
		assertEquals("1234", response.getUid());
	}

	@Test
	public void testGetByName() {
		fail("Not yet implemented");
	}

}
