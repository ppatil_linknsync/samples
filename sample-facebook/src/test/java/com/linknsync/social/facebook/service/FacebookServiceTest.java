package com.linknsync.social.facebook.service;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import sample.facebook.service.AppDetails;
import sample.facebook.service.FacebookService;
import sample.facebook.service.Logger;

/**
 * @author Paresh Patil
 * Sep 27, 2016
 * 7:33:26 PM
 */

/**
 * @author Paresh Patil
 */
// @WebAppConfiguration
@ContextConfiguration("classpath:spring/social-config2.xml")
public class FacebookServiceTest extends AbstractJUnit4SpringContextTests {

	Logger logger = new Logger(FacebookServiceTest.class);

	@Autowired
	private FacebookService facebookService;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link sample.facebook.service.FacebookService#getFacebookAppDetails()} .
	 */
	@Test
	public void testGetFacebookAppDetails() {
		AppDetails appDetails = facebookService.getFacebookAppDetails();
		logger.info("***AppDetails: " + appDetails);
	}

	/**
	 * Test method for
	 * {@link sample.facebook.service.FacebookService#getFacebookUserProfile()}
	 * .
	 */
	@Test
	public void testGetFacebookUserProfile() {
		org.springframework.social.facebook.api.User user = facebookService.getFacebookUserProfile();
		logger.info("***Facebook user full name: " + user.getName());
	}

	/**
	 * Test method for
	 * {@link sample.facebook.service.FacebookService#getFacebookFeeds()} .
	 */
	@Test
	public void testGetFacebookFeeds() {
		PagedList<Post> feeds = facebookService.getFacebookFeeds();
		logger.info("***Facebook feeds: " + feeds);
	}

}
