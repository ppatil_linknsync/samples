/**
 * @author Paresh Patil
 * Sep 20, 2016
 * 1:11:01 AM
 */
package sample.facebook.service;

import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.User;

/**
 * @author Paresh Patil
 */
public interface FacebookService {

	public AppDetails getFacebookAppDetails();

	public User getFacebookUserProfile();

	public PagedList<Post> getFacebookFeeds();
}
