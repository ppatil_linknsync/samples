/**
 * @author Paresh Patil
 * Sep 20, 2016
 * 1:11:01 AM
 */
package sample.facebook.service;

import org.springframework.context.annotation.Scope;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.facebook.connect.FacebookServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.stereotype.Service;

/**
 * @author Paresh Patil
 */
@Service
@Scope("singleton")
public class FacebookServiceImpl implements FacebookService {

	private Logger logger = new Logger(FacebookServiceImpl.class);

	String appId = "1788200068090477";
	String appSecret = "39cfc3868fec6a8599c54bde869470c0";
	String appNamespace = "linknsync-facebook";

	private FacebookServiceProvider facebookServiceProvider;
	private Facebook facebook;
	// private ConnectionRepository connectionRepository;

	// public FacebookService(Facebook facebook, ConnectionRepository
	// connectionRepository) {
	// this.facebook = facebook;
	// this.connectionRepository = connectionRepository;
	// }

	public FacebookServiceImpl() {
		facebookServiceProvider = new FacebookServiceProvider(appId, appSecret, appNamespace);
		String appToken = fetchApplicationAccessToken2();
		facebook = new FacebookTemplate(appToken, appNamespace, appId);
	}

	// @Autowired
	// public FacebookService(Facebook facebook) {
	// this.facebook = facebook;
	// // String appToken = fetchApplicationAccessToken();
	// // facebook = new FacebookTemplate(appToken);
	// }

	private String fetchApplicationAccessToken() {
		OAuth2Operations oauth = new FacebookConnectionFactory(appId, appSecret).getOAuthOperations();
		return oauth.authenticateClient().getAccessToken();
	}

	private String fetchApplicationAccessToken2() {
		String appToken = facebookServiceProvider.getOAuthOperations().authenticateClient().getAccessToken();
		logger.info("*** Facebook app token: " + appToken);
		return appToken;
	}

	public AppDetails getFacebookAppDetails() {
		// String appToken = fetchApplicationAccessToken();
		// facebook = new FacebookTemplate(appToken);
		return facebook.restOperations().getForObject(
				"https://graph.facebook.com/{appId}?fields=name,namespace,contact_email,website_url", AppDetails.class,
				appId);
	}

	public User getFacebookUserProfile() {
		// return facebook.userOperations().getUserProfile(appId);
		return facebook.restOperations().getForObject("https://graph.facebook.com/v2.5/{appId}", User.class, appId);
	}

	public PagedList<Post> getFacebookFeeds() {
		return facebook.feedOperations().getFeed(appId);
	}
}
