/**
 * @author Paresh Patil
 * Sep 28, 2016
 * 6:22:55 PM
 */
package sample.facebook.service;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Paresh Patil
 */
public final class AppDetails {

	public AppDetails() {
	}

	private long id;

	private String name;

	private String namespace;

	@JsonProperty("contact_email")
	private String contactEmail;

	@JsonProperty("website_url")
	private String websiteUrl;

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNamespace() {
		return namespace;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppDetails [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", namespace=");
		builder.append(namespace);
		builder.append(", contactEmail=");
		builder.append(contactEmail);
		builder.append(", websiteUrl=");
		builder.append(websiteUrl);
		builder.append("]");
		return builder.toString();
	}
}
