/**
 * 
 */
package sample.facebook.service;

import org.slf4j.LoggerFactory;

/**
 * @author Paresh Patil
 * 
 */
public class Logger {
	private org.slf4j.Logger logger;

	public Logger(Class<?> c) {
		logger = LoggerFactory.getLogger(c);
	}

	// private String formatLogMessageWithCorelationId(String correlationId,
	// String message) {
	// return "[" + correlationId + "] - " + message;
	// }

	public void debug(String message) {
		if (logger.isDebugEnabled())
			logger.debug(message);
	}

	public void debug(String format, Object arg) {
		if (logger.isDebugEnabled())
			logger.debug(format, arg);
	}

	public void info(String message) {
		if (logger.isInfoEnabled())
			logger.info(message);
	}

	// public void info(String correlationId, String message) {
	// if (logger.isInfoEnabled()) {
	// logger.info(formatLogMessageWithCorelationId(correlationId, message));
	// }
	// }

	public void info(String format, Object arg) {
		if (logger.isInfoEnabled())
			logger.info(format, arg);
	}

	// public void info(String correlationId, String format, Object arg) {
	// if (logger.isInfoEnabled()) {
	// formatLogMessageWithCorelationId(correlationId,
	// String.format(format, arg));
	// logger.info(format, arg);
	// }
	// }

	// public void info(String correlationId, String message, Throwable
	// throwable) {
	// if (logger.isInfoEnabled()) {
	// logger.info(
	// formatLogMessageWithCorelationId(correlationId, message),
	// throwable);
	// }
	// }

	public void info(String message, Object... args) {
		if (logger.isInfoEnabled())
			logger.info(message, args);
	}

	public void error(String message) {
		if (logger.isErrorEnabled())
			logger.error(message);
	}

	// public void error(String correlationId, String message) {
	// if (logger.isErrorEnabled()) {
	// logger.error(formatLogMessageWithCorelationId(correlationId,
	// message));
	// }
	// }

	// public void error(String correlationId, String message, Throwable
	// throwable) {
	// if (logger.isErrorEnabled()) {
	// logger.error(
	// formatLogMessageWithCorelationId(correlationId, message),
	// throwable);
	// }
	// }

	public void error(String message, Object... args) {
		if (logger.isErrorEnabled())
			logger.error(message, args);
	}
}
