/**
 * @author Paresh Patil
 * Sep 20, 2016
 * 1:27:06 AM
 */
package sample.facebook.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sample.facebook.service.FacebookService;

/**
 * @author Paresh Patil
 */
@Controller
@RequestMapping(value = "/fb")
public class FacebookController {

	@Autowired
	private FacebookService facebookService;

	private ConnectionRepository connectionRepository;

	public FacebookController() {
	}

	@Autowired
	public FacebookController(ConnectionRepository connectionRepository) {
		this.connectionRepository = connectionRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String helloFacebook(Model model) {
		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
			return "redirect:/connect/facebook";
		}

		model.addAttribute("facebookProfile", facebookService.getFacebookUserProfile());
		PagedList<Post> feed = facebookService.getFacebookFeeds();
		model.addAttribute("feed", feed);
		return "connect/facebookDetails";
	}
}
